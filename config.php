<?php

/**
 * config.php
 *
 * Configuration file
 *
 * @author     Igor Pellinen <igor.pellinen@yandex.ru>
 * @copyright  2017 Igor Pellinen
 * @version    2017-07-20
 * @link       https://bitbucket.org/igronus/cityxxi-parser
 */

return [
    // Feed URL
    'feed' => '',

    // Database settings
    'db.host' => 'localhost',
    'db.name' => '',
    'db.user' => 'root',
    'db.password' => '',
    'db.charset' => 'utf8mb4',
    'db.table' => '',
    'db.table_cost' => '',
    'db.table_params' => '',

    // Truncate table each run
    'truncate' => true,

    // Debug mode
    'debug' => false,
];
