<?php

/**
 * fields2.php
 *
 * Field rules
 *
 * @author     Igor Pellinen <igor.pellinen@yandex.ru>
 * @copyright  2017 Igor Pellinen
 * @version    2017-07-26
 * @link       https://bitbucket.org/igronus/cityxxi-parser
 */

return [
    [
        'feed' => 'statuscode',
        'variable' => 'statuscode',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_datestart',
        'variable' => 'tisa_datestart',
        'type' => 'string',
        'description' => 'Дата начала акции',
    ],

    [
        'feed' => 'tisa_dateend',
        'variable' => 'tisa_dateend',
        'type' => 'string',
        'description' => 'Дата окончания акции',
    ],

    [
        'feed' => 'tisa_description',
        'variable' => 'tisa_description',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_discountpercent',
        'variable' => 'tisa_discountpercent',
        'type' => 'decimal',
        'description' => 'Процент скидки/наценки',
    ],

    [
        'feed' => 'tisa_discounttypecode',
        'variable' => 'tisa_discounttypecode',
        'type' => 'string',
        'description' => 'Тип скидки/наценки',
    ],

    [
        'feed' => 'tisa_displayorder',
        'variable' => 'tisa_displayorder',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_name',
        'variable' => 'tisa_name',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_processingtypecode',
        'variable' => 'tisa_processingtypecode',
        'type' => 'string',
    ],

    [
        'feed' => 'transactioncurrencyid',
        'variable' => 'transactioncurrencyid',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_article.tisa_code',
        'variable' => 'tisa_article_tisa_code',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_discountsum',
        'variable' => 'tisa_discountsum',
        'type' => 'decimal',
        'description' => 'Сумма скидки/наценки',
    ],

    [
        'feed' => 'tisa_costrent',
        'variable' => 'tisa_costrent',
        'type' => 'decimal',
    ],

    [
        'feed' => 'tisa_pricerent',
        'variable' => 'tisa_pricerent',
        'type' => 'decimal',
    ],

    [
        'feed' => 'tisa_pricerentyear',
        'variable' => 'tisa_pricerentyear',
        'type' => 'decimal',
    ],
];
