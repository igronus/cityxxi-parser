city-xxi parser
================

This application is used to parse city-xxi.ru xml-feed.

Source code is available at bitbucket:
```
https://bitbucket.org/igronus/cityxxi-parser
```

Usage:

1. Get project files by git:

```
git clone https://bitbucket.org/igronus/cityxxi-parser
```

2. Create local config file:
 
```
cp config.php config-local.php
```

3. Edit config-local.php to match your configuration.

4. Create tables in your database:

```
CREATE TABLE <table_name> (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `tisa_id` varchar(255) NOT NULL,
 `statuscode` int(11) DEFAULT NULL,
 `tisa_addressid` varchar(255) DEFAULT NULL,
 `tisa_articlesubtype` varchar(255) DEFAULT NULL,
 `tisa_articletypecode` int(11) DEFAULT NULL,
 `tisa_beforebtinumber` int(11) DEFAULT NULL,
 `tisa_beforebtinumberalt` varchar(255) DEFAULT NULL,
 `tisa_code` varchar(255) DEFAULT NULL,
 `tisa_cost` decimal(14,5) DEFAULT NULL,
 `tisa_floor` int(11) DEFAULT NULL,
 `tisa_imageurl` varchar(255) DEFAULT NULL,
 `tisa_layoutid` varchar(255) DEFAULT NULL,
 `tisa_platformnumber` int(11) DEFAULT NULL,
 `tisa_price` decimal(14,5) DEFAULT NULL,
 `tisa_rooms` int(11) DEFAULT NULL,
 `tisa_sectionnumber` int(11) DEFAULT NULL,
 `tisa_spacebti` decimal(17,11) DEFAULT NULL,
 `tisa_spacebtiwobalcony` decimal(17,11) DEFAULT NULL,
 `tisa_spacedesign` decimal(17,11) DEFAULT NULL,
 `tisa_spacewobalcony` decimal(17,11) DEFAULT NULL,
 `transactioncurrencyid` varchar(255) DEFAULT NULL,
 `tisa_address.tisa_addressbuild` varchar(255) DEFAULT NULL,
 `tisa_address.tisa_addresspost` varchar(255) DEFAULT NULL,
 `tisa_address.tisa_classifierid` varchar(255) DEFAULT NULL,
 `tisa_address.tisa_floorcount` int(11) DEFAULT NULL,
 `tisa_address.tisa_name` varchar(255) DEFAULT NULL,
 `tisa_address.tisa_addressnumber` varchar(255) DEFAULT NULL,
 `tisa_layout.tisa_floors` int(11) DEFAULT NULL,
 `tisa_layout.tisa_objects` int(11) DEFAULT NULL,
 `tisa_publicity.tisa_name` varchar(255) DEFAULT NULL,
 `tisa_publicityaddress.tisa_name` varchar(255) DEFAULT NULL,
 `tisa_publicityaddress.tisa_publicitytypecode` int(11) DEFAULT NULL,
 `transactioncurrency.isocurrencycode` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `tisa_id` (`tisa_id`),
 KEY `statuscode` (`statuscode`),
 KEY `tisa_addressid` (`tisa_addressid`),
 KEY `transactioncurrency_isocurrencycode` (`transactioncurrency.isocurrencycode`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4
```

```
CREATE TABLE <table_name_cost> (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `data_id` int(11) NOT NULL,
 `statuscode` varchar(255) DEFAULT NULL,
 `tisa_datestart` varchar(255) DEFAULT NULL,
 `tisa_dateend` varchar(255) DEFAULT NULL,
 `tisa_description` varchar(255) DEFAULT NULL,
 `tisa_discountpercent` decimal(17,11) DEFAULT NULL,
 `tisa_discounttypecode` varchar(255) DEFAULT NULL,
 `tisa_displayorder` varchar(255) DEFAULT NULL,
 `tisa_name` varchar(255) DEFAULT NULL,
 `tisa_processingtypecode` varchar(255) DEFAULT NULL,
 `transactioncurrencyid` varchar(255) DEFAULT NULL,
 `tisa_article.tisa_code` varchar(255) DEFAULT NULL,
 `tisa_discountsum` decimal(17,11) DEFAULT NULL,
 `tisa_costrent` decimal(17,11) DEFAULT NULL,
 `tisa_pricerent` decimal(17,11) DEFAULT NULL,
 `tisa_pricerentyear` decimal(17,11) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4
```

```
CREATE TABLE <table_name_params> (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `data_id` int(11) NOT NULL,
 `statuscode` varchar(255) DEFAULT NULL,
 `tisa_displayorder` varchar(255) DEFAULT NULL,
 `tisa_name` varchar(255) DEFAULT NULL,
 `tisa_processingtypecode` varchar(255) DEFAULT NULL,
 `transactioncurrencyid` varchar(255) DEFAULT NULL,
 `tisa_article.tisa_code` varchar(255) DEFAULT NULL,
 `tisa_referenceinformation.tisa_code` varchar(255) DEFAULT NULL,
 `tisa_referenceinformation.tisa_displayorder` varchar(255) DEFAULT NULL,
 `tisa_referenceinformation.tisa_name` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4
```

5. Run script once to fill database:

```
/path/to/php /path/to/cityxxi-parser/parse.php
```

6. Set cron jobs:

```
0 14 * * * /path/to/php /path/to/cityxxi-parser/parse.php
0 23 * * * /path/to/php /path/to/cityxxi-parser/parse.php
```

7. Use database.

Select all flats:

```
SELECT * FROM <table_name> WHERE tisa_articletypecode = 2;
```

Select all storage rooms:

```
SELECT * FROM <table_name> WHERE tisa_articletypecode = 8;
```

Select all parkings:

```
SELECT * FROM <table_name> WHERE tisa_articletypecode = 4;
```

Select all object by address ID:

```
SELECT * FROM <table_name> WHERE tisa_addressid	= 43b26e93-c178-e511-80c5-00155dc02700;
```

Group all objects by status:

```
SELECT tisa_articletypecode, statuscode, COUNT(1) FROM <table_name> GROUP BY tisa_articletypecode, statuscode

```

Select all flats with rooms from 2 to 4 and price between 2.5M and 4M with finishing:

```
SELECT * FROM <table_name>
    LEFT JOIN <table_name_params> ON <table_name>.id = <table_name_params>.data_id
    WHERE tisa_articletypecode = 2
    AND tisa_rooms BETWEEN 2 AND 4
    AND tisa_price BETWEEN 2500000 AND 4000000
    AND (tisa_referenceinformation.tisa_code = 'os_predotd' OR tisa_referenceinformation.tisa_code = 'os_designotd');
```

Select top 25 flat prices with discount:

```
SELECT

    CASE
        WHEN
            tisa_discounttypecode = 9
        THEN
            tisa_price - (tisa_price * tisa_discountpercent)
        WHEN
            tisa_discounttypecode = 5
        THEN
            tisa_price - (tisa_spacebti * tisa_discountsum)
        WHEN
            tisa_discounttypecode = 10
        THEN
            tisa_price - tisa_discountsum
        ELSE
            tisa_price
    END AS prices

FROM <table_name>
LEFT JOIN <table_name_cost> ON <table_name>.id = <table_name_cost>.data_id
WHERE
        tisa_articletypecode = 2
    AND
        tisa_datestart > NOW()
    AND
        tisa_dateend < NOW()
ORDER BY prices DESC;
```

Select all commercial estates with rent and sale prices:

```
SELECT <table_name>.*, rent.*
LEFT JOIN <table_name_cost> as rent ON
        (<table_name>.id = rent.data_id
    AND
        (rent.tisa_costrent IS NOT NULL OR rent.tisa_pricerent IS NOT NULL))
WHERE tisa_articletypecode = 81;

```

Combine above queries to get needed filter.
