<?php

/**
 * parse.php
 *
 * Parsing feed and populating DB
 *
 * @author     Igor Pellinen <igor.pellinen@yandex.ru>
 * @copyright  2017 Igor Pellinen
 * @version    2017-07-20
 * @link       https://bitbucket.org/igronus/cityxxi-parser
 */

$config = file_exists(__DIR__ . '/config-local.php') ?
    require __DIR__ . '/config-local.php' :
    require __DIR__ . '/config.php';

$fields = require __DIR__ . '/fields.php';
$variables = $columns = [];
foreach ($fields as $field) {
    $variables[] = $field['variable'];
    $columns[] = $field['feed'];
}

$fields_cost = require __DIR__ . '/fields_cost.php';
$variables_cost = $columns_cost = [];
foreach ($fields_cost as $field) {
    $variables_cost[] = $field['variable'];
    $columns_cost[] = $field['feed'];
}

$fields_params = require __DIR__ . '/fields_params.php';
$variables_params = $columns_params = [];
foreach ($fields_params as $field) {
    $variables_params[] = $field['variable'];
    $columns_params[] = $field['feed'];
}

$dsn = sprintf("mysql:host=%s;dbname=%s;charset=%s",
    $config['db.host'], $config['db.name'], $config['db.charset']);

try {
    $pdo = new PDO($dsn, $config['db.user'], $config['db.password']);
    $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
    setlocale(LC_ALL, 'pl_PL');
} catch (Exception $e) {
    print_r(sprintf("%s\n", $e->getMessage()));
    return;
}

$xmldata = @file_get_contents($config['feed']);
if ($xmldata === false) {
    print_r(sprintf("Can't get content of %s\n", $config['feed']));
    return;
}
$xml = simplexml_load_string($xmldata);
$json = json_encode($xml);
$data = json_decode($json,TRUE);

if ($config['truncate']) {
    $sql = sprintf("TRUNCATE `%s`", $config['db.table']);
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $sql = sprintf("TRUNCATE `%s`", $config['db.table_cost']);
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    $sql = sprintf("TRUNCATE `%s`", $config['db.table_params']);
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}

foreach ($data['tisa_article'] as $record) {
    $sql = sprintf("SELECT * FROM `%s` WHERE tisa_id = :tisa_id LIMIT 1", $config['db.table']);
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':tisa_id', $record['@attributes']['id']);
    $stmt->execute();

    $rowCount = $stmt->rowCount();

    if ( ! $rowCount) {
        $sql = sprintf("INSERT INTO `%s` (tisa_id, `%s`) VALUES (:tisa_id, :%s)",
            $config['db.table'], implode('`, `', $columns), implode(', :', $variables));
        $stmt = $pdo->prepare($sql);

        $stmt->bindValue(':tisa_id', $record['@attributes']['id']);

        foreach ($variables as $key => $variable) {
            if (isset($record[$fields[$key]['feed']])) {
                if ($config['debug']) {
                    echo sprintf("Binding :%s to %s...\n", $variable, $record[$fields[$key]['feed']]);
                }

                if ($fields[$key]['type'] === 'string') {
                    $stmt->bindValue(sprintf(":%s", $variable), $record[$fields[$key]['feed']]);
                } else {
                    $stmt->bindValue(sprintf(":%s", $variable), floatval($record[$fields[$key]['feed']]));
                }
            } else {
                if ($config['debug']) {
                    echo sprintf("Binding :%s to NULL...\n", $variable);
                }

                $stmt->bindValue(sprintf(":%s", $variable),  null, PDO::PARAM_INT);
            }
        }

        if ($record['tisa_articletypecode'] == 8 && $record['tisa_articlesubtype'] == 'Нежилое') {
            if ($config['debug']) {
                echo sprintf("Rebinding :%s to %d...\n", ':tisa_articletypecode', 81);
            }

            $stmt->bindValue(':tisa_articletypecode', 81,  PDO::PARAM_INT);
        }

        $stmt->execute();
        $id = $pdo->lastInsertId();

        if ($config['debug']) {
            echo($stmt->queryString . PHP_EOL);
        }


        if (isset($record['characteristicsales'])) {
            foreach ($record['characteristicsales'] as $sale) {
                if (isset($sale['@attributes'])) {
                    parse_sale($sale, $id);
                } else {
                    foreach ($sale as $sal) {
                        parse_sale($sal, $id);
                    }
                }
            }
        }

    }
}

function parse_sale($sale, $id)
{
    if (isset($sale['tisa_datestart'])) {
        parse_cost($sale, $id);
    } else {
        parse_params($sale, $id);
    }
}

function parse_cost($sale, $id)
{
    global $config, $pdo;
    global $fields_cost, $variables_cost, $columns_cost;

    $sql = sprintf("INSERT INTO `%s` (data_id, `%s`) VALUES (:data_id, :%s)",
        $config['db.table_cost'], implode('`, `', $columns_cost), implode(', :', $variables_cost));
    $stmt = $pdo->prepare($sql);

    $stmt->bindValue(':data_id', $id);

    foreach ($variables_cost as $key => $variable) {
        if (isset($sale[$fields_cost[$key]['feed']])) {
            if ($config['debug']) {
                echo sprintf("Binding :%s to %s...\n", $variable, $sale[$fields_cost[$key]['feed']]);
            }

            if ($fields_cost[$key]['type'] === 'string') {
                $stmt->bindValue(sprintf(":%s", $variable), $sale[$fields_cost[$key]['feed']]);
            } else {
                $stmt->bindValue(sprintf(":%s", $variable), floatval($sale[$fields_cost[$key]['feed']]));
            }
        } else {
            if ($config['debug']) {
                echo sprintf("Binding :%s to NULL...\n", $variable);
            }

            $stmt->bindValue(sprintf(":%s", $variable),  null, PDO::PARAM_INT);
        }
    }

    $stmt->execute();

    if ($config['debug']) {
        echo($stmt->queryString . PHP_EOL);
    }
}

function parse_params($sale, $id)
{
    global $config, $pdo;
    global $fields_params, $variables_params, $columns_params;

    $sql = sprintf("INSERT INTO `%s` (data_id, `%s`) VALUES (:data_id, :%s)",
        $config['db.table_params'], implode('`, `', $columns_params), implode(', :', $variables_params));
    $stmt = $pdo->prepare($sql);

    $stmt->bindValue(':data_id', $id);

    foreach ($variables_params as $key => $variable) {
        if (isset($sale[$fields_params[$key]['feed']])) {
            if ($config['debug']) {
                echo sprintf("Binding :%s to %s...\n", $variable, $sale[$fields_params[$key]['feed']]);
            }

            if ($fields_params[$key]['type'] === 'string') {
                $stmt->bindValue(sprintf(":%s", $variable), $sale[$fields_params[$key]['feed']]);
            } else {
                $stmt->bindValue(sprintf(":%s", $variable), floatval($sale[$fields_params[$key]['feed']]));
            }
        } else {
            if ($config['debug']) {
                echo sprintf("Binding :%s to NULL...\n", $variable);
            }

            $stmt->bindValue(sprintf(":%s", $variable),  null, PDO::PARAM_INT);
        }
    }

    $stmt->execute();

    if ($config['debug']) {
        echo($stmt->queryString . PHP_EOL);
    }
}
