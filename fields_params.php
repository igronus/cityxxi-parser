<?php

/**
 * fields3.php
 *
 * Field rules
 *
 * @author     Igor Pellinen <igor.pellinen@yandex.ru>
 * @copyright  2017 Igor Pellinen
 * @version    2017-07-26
 * @link       https://bitbucket.org/igronus/cityxxi-parser
 */

return [
    [
        'feed' => 'statuscode',
        'variable' => 'statuscode',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_displayorder',
        'variable' => 'tisa_displayorder',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_name',
        'variable' => 'tisa_name',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_processingtypecode',
        'variable' => 'tisa_processingtypecode',
        'type' => 'string',
    ],

    [
        'feed' => 'transactioncurrencyid',
        'variable' => 'transactioncurrencyid',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_article.tisa_code',
        'variable' => 'tisa_article_tisa_code',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_referenceinformation.tisa_code',
        'variable' => 'tisa_referenceinformation_tisa_code',
        'type' => 'string',
        'description' => 'Отсюда берем отделку. Если есть os_predotd и os_designotd выводим с отделкой, если данных параметров нет, то без отделки',
    ],

    [
        'feed' => 'tisa_referenceinformation.tisa_displayorder',
        'variable' => 'tisa_referenceinformation_tisa_displayorder',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_referenceinformation.tisa_name',
        'variable' => 'tisa_referenceinformation_tisa_name',
        'type' => 'string',
    ],
];
