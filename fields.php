<?php

/**
 * fields.php
 *
 * Field rules
 *
 * @author     Igor Pellinen <igor.pellinen@yandex.ru>
 * @copyright  2017 Igor Pellinen
 * @version    2017-07-20
 * @link       https://bitbucket.org/igronus/cityxxi-parser
 */

return [
    [
        'feed' => 'statuscode',
        'variable' => 'statuscode',
        'type' => 'string',
        'description' => 'Статус объекта. Код 4 - свободно, код 8 - бронь.',
    ],

    [
        'feed' => 'tisa_addressid',
        'variable' => 'tisa_addressid',
        'type' => 'string',
        'description' => 'ID адреса',
    ],

    [
        'feed' => 'tisa_articlesubtype',
        'variable' => 'tisa_articlesubtype',
        'type' => 'string',
        'description' => 'подтип объекта',
    ],

    [
        'feed' => 'tisa_articletypecode',
        'variable' => 'tisa_articletypecode',
        'type' => 'string',
        'description' => 'код подтипа объекта. Код 8 - нежилое, кладовка. Код 2 - квартира. Код 4 - машиноместа',
    ],

    [
        'feed' => 'tisa_beforebtinumber',
        'variable' => 'tisa_beforebtinumber',
        'type' => 'string',
        'description' => 'Номер квартиры (до БТИ)',
    ],

    [
        'feed' => 'tisa_code',
        'variable' => 'tisa_code',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_cost',
        'variable' => 'tisa_cost',
        'type' => 'decimal',
        'description' => 'Цена за кв. метр.',
    ],

    [
        'feed' => 'tisa_floor',
        'variable' => 'tisa_floor',
        'type' => 'string',
        'description' => 'Этаж',
    ],

    [
        'feed' => 'tisa_imageurl',
        'variable' => 'tisa_imageurl',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_layoutid',
        'variable' => 'tisa_layoutid',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_platformnumber',
        'variable' => 'tisa_platformnumber',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_price',
        'variable' => 'tisa_price',
        'type' => 'decimal',
        'description' => 'Общая стоимость',
    ],

    [
        'feed' => 'tisa_rooms',
        'variable' => 'tisa_rooms',
        'type' => 'string',
        'description' => 'кол-во комнат',
    ],

    [
        'feed' => 'tisa_sectionnumber',
        'variable' => 'tisa_sectionnumber',
        'type' => 'string',
        'description' => 'секция',
    ],

    [
        'feed' => 'tisa_spacebti',
        'variable' => 'tisa_spacebti',
        'type' => 'decimal',
        'description' => 'Площадь по БТИ',
    ],

    [
        'feed' => 'tisa_spacebtiwobalcony',
        'variable' => 'tisa_spacebtiwobalcony',
        'type' => 'decimal',
        'description' => 'Площадь по БТИ без учета балконов/лоджии',
    ],

    [
        'feed' => 'tisa_spacedesign',
        'variable' => 'tisa_spacedesign',
        'type' => 'decimal',
        'description' => 'Площадь по проекту',
    ],

    [
        'feed' => 'tisa_spacewobalcony',
        'variable' => 'tisa_spacewobalcony',
        'type' => 'decimal',
        'description' => 'Площадь по проекту без учета балконов/лоджий',
    ],

    [
        'feed' => 'transactioncurrencyid',
        'variable' => 'transactioncurrencyid',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_address.tisa_addressbuild',
        'variable' => 'tisa_address_tisa_addressbuild',
        'type' => 'string',
        'description' => 'Строительный адрес объекта',
    ],

    [
        'feed' => 'tisa_address.tisa_addresspost',
        'variable' => 'tisa_address_tisa_addresspost',
        'type' => 'string',
        'description' => 'Почтовый адрес объекта',
    ],

    [
        'feed' => 'tisa_address.tisa_classifierid',
        'variable' => 'tisa_address_tisa_classifierid',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_address.tisa_floorcount',
        'variable' => 'tisa_address_tisa_floorcount',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_address.tisa_name',
        'variable' => 'tisa_address_tisa_name',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_address.tisa_addressnumber',
        'variable' => 'tisa_address_tisa_addressnumber',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_layout.tisa_floors',
        'variable' => 'tisa_layout_tisa_floors',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_layout.tisa_objects',
        'variable' => 'tisa_layout_tisa_objects',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_publicity.tisa_name',
        'variable' => 'tisa_publicity_tisa_name',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_publicityaddress.tisa_name',
        'variable' => 'tisa_publicityaddress_tisa_name',
        'type' => 'string',
    ],

    [
        'feed' => 'tisa_publicityaddress.tisa_publicitytypecode',
        'variable' => 'tisa_publicityaddress_tisa_publicitytypecode',
        'type' => 'string',
    ],

    [
        'feed' => 'transactioncurrency.isocurrencycode',
        'variable' => 'transactioncurrency_isocurrencycode',
        'type' => 'string',
    ],
];
